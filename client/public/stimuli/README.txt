#---- stimuli images for state representation experiment
# March 15, 2022

file names are organised as follows: 
- first letter: b = beach or f = forest
- second letter: p = pointy roof or f = flat roof
- third letter: i = isolated or b = in a bundle
- number to indicate several stimuli of same category