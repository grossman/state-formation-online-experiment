#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 10:20:34 2022

@author: grossman
"""

# Python program to read
# json file


import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_json (r'/Users/grossman/Downloads/FABI_v1.json')
df = pd.read_json (r'/Users/grossman/Downloads/LUIANTA.json')
df = pd.read_json (r'/Users/grossman/Downloads/NICO.json')
df = pd.read_json (r'/Users/grossman/Downloads/JEREMY.json')
df = pd.read_json (r'/Users/grossman/Downloads/Lydia.json')
#df = pd.read_json (r'/Users/grossman/Downloads/INBAL.json')
#df = pd.read_json (r'/Users/grossman/Downloads/NOA.json')
df = pd.read_json (r'/Users/grossman/Downloads/SAM.json')
df = pd.read_json (r'/Users/grossman/Downloads/test2.json')
### Reorganize the data to have one row per trial ###
data = df[(df.type=='houses') & (df.is_training==0) ].copy()  
ntrials = data.shape[0]


logs = ['/Users/grossman/Downloads/SAM.json',
        '/Users/grossman/Downloads/JEREMY.json',
        '/Users/grossman/Downloads/NOA.json']

acc_all = np.empty((len(logs),380))

for i_sub in range(len(logs)):

    df = pd.read_json (logs[i_sub])
    ### Reorganize the data to have one row per trial ###
    data = df[(df.type=='houses') & (df.is_training==0) ].copy()  
    ntrials = data.shape[0]
        
    #prts = pd.read_json(r'//Users/grossman/Projects/STATE FORMATION/state-formation-online-experiment/client/public/protocols/prt.json')
    #prt = pd.read_csv(r'//Users/grossman/Projects/STATE FORMATION/state-formation-online-experiment/client/public/protocols/prt54.csv')
    # =============================================================================Make sure to save ISI and ITI trials!! (not save in lui's version)
    # #add ISI
    # df_ISI = df[df.type=='ISI']
    # idx = np.arange(0,df_ISI.shape[0],2)
    # data.ISI = np.asarray(df_ISI.iloc[idx].ISI)
    # 
    # #add ITI
    # data.ITI = np.asarray(df[df.type=='ITI'].ITI)
    # 
    # =============================================================================
    #add is_correct
    data.is_correct = np.asarray(df[(df.type == 'reward') & (df.is_training==0)].is_correct)
    
    #add client type (-1-1 /-11/1-1/11)
    client_type = []
    for i in range(0,ntrials):
        if data.iloc[i].client_f1==-1:
            client_type.append("-1")
        elif data.iloc[i].client_f1==2:
                client_type.append("2")
        else: print("client type could not be assigned")
    data['client_type'] = client_type  
      
    #add lhouse type (-1-1 /-11/1-1/11)
    lhouse_type = []
    for i in range(0,ntrials):
        if data.iloc[i].lhouse_f1==-1 and data.iloc[i].lhouse_f2==-1:
            lhouse_type.append("-1,-1")
        elif data.iloc[i].lhouse_f1==-1 and data.iloc[i].lhouse_f2==1:
                lhouse_type.append("-1,1")
        elif data.iloc[i].lhouse_f1==1 and data.iloc[i].lhouse_f2==-1:
                lhouse_type.append("1,-1")
        elif data.iloc[i].lhouse_f1==1 and data.iloc[i].lhouse_f2==1:
                lhouse_type.append("1,1")
        else: print("lhouse type could not be assigned")
    data['lhouse_type'] = lhouse_type 
        
        
    #add rhouse type (-1-1 /-11/1-1/11)
    rhouse_type = []
    rhouse_f1 = data.rhouse_f1 #remove in newer version - was a bug in col name
    for i in range(0,ntrials):
        if data.iloc[i].rhouse_f1==-1 and data.iloc[i].rhouse_f2==-1:
            rhouse_type.append("-1,-1")
        elif data.iloc[i].rhouse_f1==-1 and data.iloc[i].rhouse_f2==1:
                rhouse_type.append("-1,1")
        elif data.iloc[i].rhouse_f1==1 and data.iloc[i].rhouse_f2==-1:
                rhouse_type.append("1,-1")
        elif data.iloc[i].rhouse_f1==1 and data.iloc[i].rhouse_f2==1:
                rhouse_type.append("1,1")
        else: print("rhouse type could not be assigned")
    data['rhouse_type'] = rhouse_type 
    
    
    for i in range(0,ntrials):
        if rhouse_type[i]==lhouse_type[i]:
            print('trial' + str(i) + ': same rhouse and lhouse')
        
    ### Assign state id to each trial ###
    state = []
    state_comps = []
    state_val = []
    istate=-1
    state_per_trial = np.zeros(ntrials)
    for iclient, client in enumerate(np.unique(data.client_type)):
        for ilhouse, lhouse in enumerate(np.unique(data.lhouse_type)):
            for irhouse, rhouse in enumerate(np.unique(data.rhouse_type)):
                if lhouse!=rhouse:
                    istate = istate+1
                    c_state_trials = np.array(data[((data.client_type==client) & (data.lhouse_type==lhouse)) & (data.rhouse_type==rhouse)].trial)
                    c_state_trials = c_state_trials.astype(int)
                    if len(c_state_trials)==0:
                        print('no trials found for state' + str(istate))
                    else:
                        state_per_trial[c_state_trials] = int(istate)
                        assert(len(np.unique(np.asarray(data.iloc[c_state_trials].reward_diff)))==1)
                        assert(np.equal(c_state_trials, np.array(data.iloc[c_state_trials].trial)).all())
                        state.append(istate)
                        state_comps.append(client + "," + lhouse + "," +rhouse)
                        state_val.append(np.unique(np.asarray(data.iloc[c_state_trials].reward_diff))[0])
                
                
                
    data['state'] = state_per_trial 
    state = np.array(state)
    ### Compute performance and RT in a runing window of x trials ###
    is_correct = np.array(data.is_correct)*100
    is_correct[is_correct==-200]=np.NaN
    is_correct = pd.Series(is_correct)
    rt = np.array(data.rt)
    rt = pd.Series(rt)
    fig, ax = plt.subplots(1,3,figsize=(12, 6))
    
    fig.tight_layout(pad =5.0)
    if "subject" in data:
        fig.suptitle(data.iloc[0].subject, fontsize=14)
        #fig.suptitle('NS', fontsize=14)
   
    is_correct_wo_nan = is_correct[~np.isnan(is_correct)]
    ax[0].plot(is_correct_wo_nan.rolling(10).mean(), color = 'c')
    ax[0].set_xlabel('trial #', fontsize=10)
    ax[0].set_ylabel('% correct', fontsize=10)
    ax[0].set_title('accuracy', fontsize=12)
    ax[0].set_xticks(np.arange(0,ntrials,50))
    ax[0].set_xticklabels(np.arange(0,ntrials,50),fontsize=7)
    ax[0].set_yticks(np.arange(0,100,20))
    ax[0].set_xlim((0, ntrials))
    
    acc_all[i_sub,:] = np.array(is_correct_wo_nan.rolling(15).mean())[:380]
    
    #ax[0,1].rt.rolling(10).mean())
    
    
    ### Compute performance per state ###
    from matplotlib.cm import ScalarMappable
    state_sorted =  np.unique(state)
    correct_by_state = np.zeros((len(state_sorted),np.max(np.bincount(state_per_trial.astype(int)))))
    correct_by_state[:]=np.nan
    
    ax[1].set_prop_cycle(plt.cycler('color', plt.cm.viridis(np.linspace(0, 1, len(state)))))
    for istate, state_id in enumerate(state_sorted):
        ts = np.array(is_correct[np.array(data[data.state==state_id].trial)])
        ts = ts[0:int(len(ts)/4)]
        correct_by_state[istate,:len(ts)]= ts
        print(len(ts))
        #ax[1].plot(ts) #add running mean
        
    ax[1].set_xlabel('state (by value diff)', fontsize=10)
    ax[1].set_ylabel('% correct', fontsize=10)
    ax[1].set_title('mean accuracy by state', fontsize=12)
    #ax[0,0].set_xticks(np.arange(0,ntrials,100))
    #ax[0,0].set_yticks(np.arange(0,100,20))
    x = np.arange(0,24)
    data_color = np.sort(np.abs(state_val))
    data_color_normalized = [x / max(data_color) for x in data_color]
    my_cmap = plt.get_cmap("viridis")
    colors = my_cmap(data_color_normalized)
    ax[1].bar(x,np.nanmean(correct_by_state,axis=1),color = colors)
    ax[1].set_xticks(x)
    ax[1].set_xticklabels(np.sort(np.abs(state_val)).astype(int),fontsize=7, rotation=-45)
    
    
    #plot ongoing acc per state
    ax[2].set_prop_cycle(plt.cycler('color', plt.cm.viridis(np.linspace(0, 1, len(state)))))
    for istate, state_id in enumerate(state_sorted):
        tmp = data[data.state==state_id].is_correct*100
        y = tmp.rolling(5).mean()
        
        ax[2].plot(np.arange(0,y.shape[0],1),y) #add running mean
    ax[2].set_xlabel('# state repitition', fontsize=10)
    ax[2].set_title('ongoing accuracy by state', fontsize=12)
    #ax[2].set_xticks(np.arange(0,ntrials,50))
    #ax[2].set_xticklabels(fontsize=7)
    
    
#%% plt mean accuracy

fig_mean, ax_mean = plt.subplots(1,1, sharex = 'col', sharey = 'row', figsize = (5, 5))  
ax_mean.set_xlabel('trial #',fontsize=18,fontname='Arial')
ax_mean.set_ylabel('proportion correct',fontsize=18,fontname='Arial')
acc_mean = np.nanmean(acc_all/100,axis=0)
acc_se = np.nanstd(acc_all/100,axis=0)/np.sqrt(len(logs))
ax_mean.plot(acc_mean, color='darkcyan', linewidth=2)
ax_mean.fill_between(np.array(range(len(acc_mean))), acc_mean-acc_se, acc_mean+acc_se, facecolor = 'darkcyan', alpha = 0.2)  
ax_mean.set_xlim(0,1000)#len(acc_mean_wo_exploration))
ax_mean.set_ylim(0,1.02)
fig_mean.savefig('/Users/grossman/Downloads/accmean.pdf' , format='pdf')
    
