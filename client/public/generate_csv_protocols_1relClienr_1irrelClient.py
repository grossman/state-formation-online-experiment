#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 20:25:36 2022

@author: grossman
"""

import os
import pandas as pd
import numpy as np
import random
from itertools import product
from itertools import repeat
import pandas as pd
import csv
import json
from collections import OrderedDict

simple_stim = True
balanceDesign_QandD = False #quick and dirty - just make sure that within a blovk eah irrel feature is  -1 in app. half and of trials and 1 in the other half
balanceDesign = True 
n_prts = 100 # this will change is balanceDesign is set to true
n_eps =2 # how many episodes to include in the prt?

if simple_stim: stim_suf = '.png'
else: stim_suf = '.jpg'

def convert_stim_2features_to_type(i):
    switcher={
             '[-1-1]': 0,
             '[-11]': 1,
             '[1-1]': 2,
             '[11]': 3,
          }
    return switcher.get(i,"Invalid input")

def convert_stim_3features_to_type(i):
    switcher={
             '[-1-1-1]': 0,
             '[-1-11]': 1,
             '[-11-1]': 2,
             '[-111]': 3,
             '[1-1-1]': 4,
             '[1-11]': 5,
             '[11-1]': 6,
             '[111]': 7
          }
    return switcher.get(i,"Invalid input")


 
# %% define reward function 
# this defines how the reward depends on the 3 features of the client and the 3 features of the selected house
reward_weights = np.array([1, 0, 1, 3, 0]) # first 2 weights - for stageA features, last 3 weights for stageB features 

# make choicemap: mask inputs based on each of 4 possible choices
choicemap = np.empty((2, 8)) #row per action: pos 0-1:client features, pos 2-4: lhouse feats, pos 5-7: rhouse feats
choicemap[0, :] = np.array([1,1,1,1,1,0,0,0])# left house
choicemap[1, :] = np.array([1,1,0,0,0,1,1,1])# right house

def rewardfun(cinputs, selected_action, reward_weights, do_prob): 
    cidx = np.flatnonzero(choicemap[selected_action, :])
    R = np.dot(cinputs[cidx[np.arange(0, 2)]], reward_weights[0:2]) \
        * np.dot(cinputs[cidx[np.arange(2, 2+3)]], reward_weights[2:]) 
    R = int((R+8)*(100/16))
    R_prob = np.random.normal(loc=R, scale=0.5, size=None) 
    if do_prob: R = R_prob
    return(R)
    
    
# make all satgeA possible inputs 
cue_inputs= np.array([x for x in product([-1, 2], repeat=2)])

# make all possible irrel variant of a given state
state_variants = np.array([x for x in product([-1, 1], repeat=3)]) 
#first var is the irrel feat of the client, 2 is the one of the left house 
#and 3rd is the one of the right house
state_variants[np.where(state_variants[:,0]==1),0]=2 


#%% make inputs by combining all stage B choice options
options = np.array([x for x in product([-1, 1], repeat=3)])
options = options.reshape((4,2,3)) #all possible options for a single item (A/B)
inputs = np.zeros((4*3*4, 6)) # set array with all possible inputs (each row is an input for a single decision stage (i.e. itemA/itemB); entries 0-3 represent compA, entries 4-8 represent compB )
inputs_type =  np.zeros((4*3*4)) #index the selected 2 items
trial_idx = 0
for acomp in np.arange(0, 4):  
    for idx, bcomp in enumerate(np.setdiff1d(np.arange(0, 4), acomp)):
        ct = acomp*12 + idx*4 # start row idx of this batch of options 
        trial_idx = trial_idx+1
        tmp = np.meshgrid(np.arange(0,2), np.arange(0,2))
        trials = np.stack((options[acomp,tmp[0].flatten(),:],options[bcomp,tmp[1].flatten(),:]),axis=1)
        np.random.shuffle(trials)
        inputs[range(ct, ct+4), :] = trials.reshape(4, 6)
        inputs_type[range(ct, ct+4)] = trial_idx*np.ones([4])
        
# set all posible combinations of input type in stage a and input type (house pair) in stage b (trialcomps)
tmp = np.meshgrid(np.arange(0, cue_inputs.shape[0]), np.arange(0, inputs.shape[0]))
trialcomps = np.stack((tmp[0].flatten(), tmp[1].flatten()))
maxtrials = trialcomps.shape[1] # this includes for each cue type (out of 4) - showing the same pair 4 times (under 4 different irrel feature combs)
trialstates = np.empty(maxtrials)
f_client = np.empty(maxtrials)
f_lhouse = np.empty(maxtrials)
f_rhouse = np.empty(maxtrials)
state_version = np.empty(maxtrials) # 8 possible version given the combs of 3 irrel feats
i_state=-1
for i_cue, cue in enumerate(np.unique(cue_inputs)) :
    cue_mask = cue_inputs[trialcomps[0,:],0]==cue
    for i_lhouse in range(options.shape[0]):
        lhouse_mask = (inputs[trialcomps[1,:],0:2]==options[i_lhouse][0][0:2]).all(axis=1)
        for i_rhouse in range(options.shape[0]):
            if i_lhouse!=i_rhouse: 
                i_state=i_state+1
                rhouse_mask = (inputs[trialcomps[1,:],3:5]==options[i_rhouse][0][0:2]).all(axis=1)
                state_mask = cue_mask & lhouse_mask & rhouse_mask
                trialstates[state_mask] = i_state
                f_client[state_mask] = cue_inputs[trialcomps[0,state_mask],1]
                f_lhouse[state_mask] = inputs[trialcomps[1,state_mask],2]
                f_rhouse[state_mask] = inputs[trialcomps[1,state_mask],5]
                
#log state version (8 variants based on irrel feat combs:
irrel_feats = np.array([cue_inputs[trialcomps[0,:],[1]],
               inputs[trialcomps[1,:],[2]],
               inputs[trialcomps[1,:],[5]]]) #irrel feat valus X trials   

for i_version in range(state_variants.shape[0]):
   c_version_mask = np.where((irrel_feats.astype('int').T == state_variants[i_version,:]).all(axis=1))[0]
   assert(len(c_version_mask)==24)
   state_version[c_version_mask] = i_version
state_version = state_version.astype('int')


#%% generate design sequences 
if balanceDesign_QandD: #organize each episode in blocks of 48 trials -  
#2 rep per states, and each of the irrelevant feaures =-1 on half of the trial and -1 on the other half
    ep_seqs = []
    n_attempts=10000
    validPRT = np.empty(n_attempts)
    for i_prt in range(n_attempts):
        print("prt " + str(i_prt))
        prt = np.zeros(maxtrials).astype(int)
        used = np.zeros(maxtrials)
        dist_f_client = np.empty(4)
        dist_f_lhouse = np.empty(4)
        dist_f_rhouse = np.empty(4)
        for i_block in range(4):
            prt_cblock = np.zeros(48)
            #select 48 trials, 2 reps per state, 
            for i_state in range(24):
                idx = np.intersect1d(np.where(trialstates==i_state)[0],np.where(used==0)[0])
                random.shuffle(idx)
                used[idx[0:2]]=1
                prt_cblock[i_state*2:i_state*2+2]=idx[0:2]
            
            random.shuffle(prt_cblock)
            prt[i_block*48:i_block*48+48]=prt_cblock
            dist_f_client[i_block] = np.max([sum(f_client[prt_cblock.astype(int)].astype(int)==-1),sum(f_client[prt_cblock.astype(int)].astype(int)==2)])
            dist_f_lhouse[i_block]  = np.max([sum(f_lhouse[prt_cblock.astype(int)].astype(int)==-1),sum(f_lhouse[prt_cblock.astype(int)].astype(int)==1)])
            dist_f_rhouse[i_block]  = np.max([sum(f_rhouse[prt_cblock.astype(int)].astype(int)==-1),sum(f_rhouse[prt_cblock.astype(int)].astype(int)==1)])
        #check for balanced dist of irrel feats 
        a = np.concatenate((dist_f_client,dist_f_lhouse,dist_f_rhouse))
        if any(a>26): 
            validPRT[i_prt] = False
        else: 
            validPRT[i_prt] = True
            ep_seqs.append(prt)
        n_prts = int(np.floor(len(ep_seqs)/n_eps))
           
if balanceDesign: 
#each state is presented under 8 variations, the order of presented variation is as follows:
# 1,2,3,4,5,6,7,8 for the first 3 rounds of states; 2,3,4,5,6,7,8,1 for the 2nd 3 rounds and so on (presentation of variant is shifted by 1 every 3 presentations):
    prt_seqs = []
    for i_prt in range(n_prts):
        prt = np.zeros(maxtrials*n_eps)
        for i_ep in range(n_eps):
            used = np.zeros(maxtrials) #for validation
            shift=0
            prt_ep = np.zeros(maxtrials)
            for i_block in range(int(maxtrials/48)):
                prt_cblock = np.zeros(48)
                prt_cblock_states = np.repeat(np.arange(0,24,1),2)
                random.shuffle(prt_cblock_states)
                #now assign actuall version (i.e. full tiral descriptor) to states
                for i_state_repeat in range(2):
                    for i_state in range(24):
                        c_state_idx = np.where(prt_cblock_states==i_state)[0][i_state_repeat]
                        c_version = (i_state%8+shift)%8
                        idx_tmp = np.intersect1d(np.where(trialstates==i_state)[0],np.where(used==0)[0])
                        idx = np.intersect1d(idx_tmp,np.where(state_version==c_version)[0])
                        assert(len(idx)==1)
                        used[idx]=used[idx]+1
                        prt_cblock[i_state  + i_state_repeat*24]=idx
                        if i_state==23: shift = shift+1
                prt_ep[48*i_block:48*i_block+48]=prt_cblock
            assert((used==1).all()) 
            prt[maxtrials*i_ep:maxtrials*i_ep + maxtrials] = prt_ep
        prt_seqs.append(prt)
      
                
                
#%%              

# assign specific exemplars to each trials
stim_clients = [['client_-1_-1a','client_-1_1a','client_1_-1a','client_1_1a'],
                ['client_-1_-1b','client_-1_1b','client_1_-1b','client_1_1b'],
                ['client_-1_-1c','client_-1_1c','client_1_-1c','client_1_1c'],
                ['client_-1_-1d','client_-1_1d','client_1_-1d','client_1_1d']]

stim_houses = [['house_-1_-1_-1a','house_-1_-1_1a','house_-1_1_-1a','house_-1_1_1a','house_1_-1_-1a','house_1_-1_1a','house_1_1_-1a','house_1_1_1a'],
              ['house_-1_-1_-1b','house_-1_-1_1b','house_-1_1_-1b','house_-1_1_1b','house_1_-1_-1b','house_1_-1_1b','house_1_1_-1b','house_1_1_1b'],
              ['house_-1_-1_-1c','house_-1_-1_1c','house_-1_1_-1c','house_-1_1_1c','house_1_-1_-1c','house_1_-1_1c','house_1_1_-1c','house_1_1_1c'],
              ['house_-1_-1_-1d','house_-1_-1_1d','house_-1_1_-1d','house_-1_1_1d','house_1_-1_-1d','house_1_-1_1d','house_1_1_-1d','house_1_1_1d']]


# generate 100 random protocols, composed of 2 episodes, each consists of all possible trials (in a shuffled order)
n_trials = n_eps*maxtrials #384 #how many trials to include (overrides the n_eps)
assert(n_trials<=n_eps*maxtrials)
for i_prt in range(0,n_prts):
    prt_client = []
    prt_lhouse = []
    prt_rhouse= []
    prt_lhouse_reward= []
    prt_rhouse_reward = []
    prt_diff_reward = []
    prt_correct = []
    prt_max_reward = []
    prt_min_reward = []
    prt_ISI = np.repeat(np.arange(400,600,50), int(maxtrials*n_eps/len(np.arange(400,600,50)))) # time between client and houses
    np.random.shuffle(prt_ISI)
    prt_ITI = np.repeat(np.arange(1000,1500,50), int(maxtrials*n_eps/len(np.arange(1000,1500,50)))+2) # time between reward and next trial onset
    np.random.shuffle(prt_ITI)
    
    prt_clientf1 = []
    prt_clientf2 = []
    prt_clientf3 = []
    prt_client_exemp = []
    prt_client_value =[]
    
    prt_lhousef1 = []
    prt_lhousef2 = []
    prt_lhousef3 = []
    prt_lhouse_exemp = []
    prt_lhouse_value =[]
    
    prt_rhousef1 = []
    prt_rhousef2 = []
    prt_rhousef3 = []
    prt_rhouse_exemp = []
    prt_rhouse_value = []
    


    #each house/client will apear 48 times during a full epsiode. We will create a shuffeled vector indicating which 
    #of the 4 exemplars to present on each trial
    n_repeats_per_type = sum(trialcomps[0,:]==1) #true for cliets only, houses will be twice as many
    exemplar_str = [0,1,2,3] #each client/house type has 4 distinct exemplars
    n_repeats_per_exemplar = int(n_repeats_per_type/ len(exemplar_str))
    exemplar_clients_prt = exemplar_str*n_repeats_per_exemplar*2
    random.shuffle(exemplar_clients_prt)
    exemplar_houses_prt = exemplar_str*n_repeats_per_exemplar*2
    random.shuffle(exemplar_houses_prt)

    stim_client_type_count = np.zeros(8, 'int')
    stim_house_type_count = np.zeros(8, 'int')
    
    
    for ep in range(0,n_eps):
        ctrialcomps = np.copy(trialcomps)
        shuffeled_idx = np.arange(0,trialcomps.shape[1])
        np.random.shuffle(shuffeled_idx)
        if balanceDesign_QandD:
            shuffeled_idx = ep_seqs[i_prt*n_eps+ep]
        if balanceDesign:
            shuffeled_idx = prt_seqs[i_prt][ep*maxtrials:ep*maxtrials+maxtrials].astype('int')
        
        for trial in range(0,maxtrials): #maxtrials):
            if trial < n_trials//n_eps:
                c_cuecomp = trialcomps[0,shuffeled_idx[trial]]
                stim_client_type_count[c_cuecomp] =  stim_client_type_count[c_cuecomp]+1
                prt_client.append('stim/' + stim_clients[int(exemplar_clients_prt[stim_client_type_count[c_cuecomp]-1])][c_cuecomp]+ stim_suf)
                if cue_inputs[c_cuecomp,0]==-1:
                    assert(prt_client[-1][12]=='-')
                elif  cue_inputs[c_cuecomp,0]==1:
                    assert(prt_client[-1][12]=='1')
                
                c_housecomp = trialcomps[1,shuffeled_idx[trial]]
                
                c_lhouse = inputs[c_housecomp,0:3]
                c_lhouse = c_lhouse.astype(int)     
                c_lhouse_type = convert_stim_3features_to_type(str(c_lhouse).replace(" ", ""))
                stim_house_type_count[c_lhouse_type] =  stim_house_type_count[c_lhouse_type]+1
                prt_lhouse.append('stim/' + stim_houses[int(exemplar_houses_prt[stim_house_type_count[c_lhouse_type]-1])][c_lhouse_type]+ stim_suf)

                                  
                c_rhouse = inputs[c_housecomp,3:6]
                c_rhouse = c_rhouse.astype(int)  
                c_rhouse_type = convert_stim_3features_to_type(str(c_rhouse).replace(" ", ""))
                stim_house_type_count[c_rhouse_type] =  stim_house_type_count[c_rhouse_type]+1
                prt_rhouse.append('stim/' + stim_houses[int(exemplar_houses_prt[stim_house_type_count[c_rhouse_type]-1])][c_rhouse_type]+ stim_suf)
                
                assert(prt_rhouse[-1] != prt_lhouse[-1])
                #prt_rhouse.append('stim/house_' + str(c_rhouse[0])+ '_' +  str(c_rhouse[1]) + '_' + 
                #                 str(c_rhouse[2]) + exemplar_str[int(np.round(np.random.rand(1)))]+'.jpg')
                
                #compute reward for lhouse and for rhouse
                cinputs = np.concatenate((cue_inputs[c_cuecomp,:],inputs[c_housecomp,:]),axis=0)
                lhouse_reward = int(rewardfun(cinputs, 0, reward_weights, do_prob=False))
                rhouse_reward = int(rewardfun(cinputs, 1, reward_weights, do_prob=False))
                prt_lhouse_reward.append(lhouse_reward)
                prt_rhouse_reward.append(rhouse_reward)
                prt_max_reward.append(np.max([lhouse_reward,rhouse_reward]))
                prt_min_reward.append(np.min([lhouse_reward,rhouse_reward]))
                diff_reward = lhouse_reward - rhouse_reward
                prt_diff_reward.append(int(diff_reward))
                if diff_reward>0:
                    prt_correct.append('lhouse')
                else: 
                    prt_correct.append('rhouse')
                    
                #log individual features values
                prt_clientf1.append(cinputs[0])
                prt_clientf2.append(cinputs[1])
                prt_client_exemp.append(prt_client[-1][-5])
                
                prt_lhousef1.append(cinputs[2])
                prt_lhousef2.append(cinputs[3])
                prt_lhousef3.append(cinputs[4])
                prt_lhouse_exemp.append(prt_lhouse[-1][-5])
                
                prt_rhousef1.append(cinputs[5])
                prt_rhousef2.append(cinputs[6])
                prt_rhousef3.append(cinputs[7])
                prt_rhouse_exemp.append(prt_rhouse[-1][-5])
            
            
                                     
      
    #save prt to csv 
    prt_path =  '/Users/grossman/Projects/STATE FORMATION/state-formation-online-experiment/client/public/protocols_1relClient_1irrelClient/'  
    if not(os.path.isdir(prt_path)): os.mkdir(prt_path) 

    
    # dictionary of lists
    prt_ITI = prt_ITI[:len(prt_client)]
    prt_ISI = prt_ISI[:len(prt_client)]
    dict = {'client_stim': prt_client[:n_trials], 'lhouse_stim': prt_lhouse[:n_trials], 'rhouse_stim': prt_rhouse[:n_trials], 
            'lhouse_reward': prt_lhouse_reward[:n_trials],'rhouse_reward': prt_rhouse_reward[:n_trials], 'diff_reward': prt_diff_reward[:n_trials],
            'correct': prt_correct, 'ISI': prt_ISI, 'ITI': prt_ITI[:n_trials],
            'client_f1': prt_clientf1, 'client_f2': prt_clientf2, 'client_exemplar': prt_client_exemp,
            'lhouse_f1': prt_lhousef1, 'lhouse_f2': prt_lhousef2, 'lhouse_f3': prt_lhousef3, 'lhouse_exemplar': prt_lhouse_exemp,
            'rhouse_f1': prt_rhousef1, 'rhouse_f2': prt_rhousef2, 'rhouse_f3': prt_rhousef3, 'rhouse_exemplar': prt_rhouse_exemp,
            'min_reward': prt_min_reward, 'max_reward': prt_max_reward[:n_trials], 'is_training':list(np.zeros((n_trials)))}
    df = pd.DataFrame(dict)
    	
    # saving the dataframe as csv
    df.to_csv(prt_path + 'prt' + str(i_prt) + '.csv')
    
    
    
    
#load all the saved CSVs and push them into one jason file:
fieldnames = ("trial","client_stim","lhouse_stim","rhouse_stim", "lhouse_reward", 
              "rhouse_reward", 'diff_reward', 'correct', 'ISI', 'ITI',
              "client_f1", "client_f2", "client_exemplar",
              "lhouse_f1" , "lhouse_f2" , "lhouse_f3", "lhouse_exemplar",
              'rhouse_f1', 'rhouse_f2', 'rhouse_f3', 'rhouse_exemplar', 'min_reward', 'max_reward','is_training')

import glob
csv_files = sorted(glob.glob(prt_path + '*.csv'))
assert(len(csv_files)==n_prts)
allentries = []
for i_prt, prt in enumerate(csv_files):
    entries = []
    #the with statement is better since it handles closing your file properly after usage.
    with open(prt, 'r') as csvfile:
        #python's standard dict is not guaranteeing any order, 
        #but if you write into an OrderedDict, order of write operations will be kept in output.
        reader = csv.DictReader(csvfile, fieldnames)
        next(reader) #skip first line
        for row in reader:
            entry = OrderedDict()
            for field in fieldnames:
                entry[field] = row[field]
            entries.append(entry)
    allentries.append(entries)
    
with open(prt_path + 'prt' +  '.json', 'w') as jsonfile:
    json.dump(allentries , jsonfile)
    jsonfile.write('\n')

        
    
# append the name of the variable and apostrophes
f = open(prt_path + 'prt.json', "r+")
js = f.read()
f.seek(0,0)
f.write("var prt =" + js )
f.close()




            
        