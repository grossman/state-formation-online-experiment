#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 17:17:10 2022

@author: grossman
"""

import os 
import glob 
import matplotlib.pyplot as plt
import sys
from PIL import Image




stim_dir= 'stimuli/setB_beach:forest_pointy:flat_wooden:brick_preprocessed'
#stim_dir = 'stimuli/setA_clients_young:old_male:female_happy:sad_preprocessed'
cur_dir = os.path.dirname(__file__)
stim_path = os.path.join(cur_dir, stim_dir)
categories = glob.glob(stim_path + "/*/")
categories_names = [os.path.basename(os.path.normpath(x)) for x in categories]
categories_names = [x.replace('_', ' ') for x in categories_names]

fig, ax = plt.subplots(len(categories),figsize=(15, 15))
plt.axis('off')


#generate concataned images per category
for i_cat, cat_path in enumerate(categories):
    cur_cat_image_names = glob.glob(categories[i_cat] + "/house*.jpg")

    images = [Image.open(x) for x in cur_cat_image_names]
    widths, heights = zip(*(i.size for i in images))
    
    total_width = sum(widths)
    max_height = max(heights)
    
    concat_im = Image.new('RGB', (total_width, max_height))
    
    x_offset = 0
    for im in images:
      concat_im.paste(im, (x_offset,0))
      x_offset += im.size[0]
    
    concat_im.save(categories[i_cat] + '/concat.jpg')
    
    ax[i_cat].imshow(concat_im)
    ax[i_cat].set_title(categories_names[i_cat], fontsize=12)
    ax[i_cat].axis('off')
    
fig.tight_layout()
plt.show() 
fig.savefig(stim_path + '/gallery.jpg')
   

    