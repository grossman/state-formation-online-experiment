#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 26 10:42:57 2022

@author: grossman
"""
import os
import glob 
from PIL import Image


basewidth = 310 #the desired width of the resized image in pixels


dirname = os.path.dirname(__file__)
stim_path = os.path.join(dirname, 'stim/')
stim_files = glob.glob(stim_path + "house*.png")
assert(len(stim_files)==32)


for i_stim, stim in enumerate(stim_files):
        
    img = Image.open(stim)
    wpercent = (basewidth / float(img.size[0])) #how much should we change the height to keep the original proportions
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    img.save(stim)
    

