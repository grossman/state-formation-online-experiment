
//var not_supported_text = '<p>Due to browser compatibility issues, it is only possible to complete the experiment using Google Chrome or Firefox.</p>'+
//    '<p>Please press the button below to return to Prolific and start the experiment again using one of these two browsers.</p>';
function get_consent(simple_stim) {
  if (simple_stim){
    var consent_text = "<h2>Study Information and Statement of informed Consent</h2>" +
        "<h3>1. Aim of the study</h3>" +
        "<p align='left'> In this study we want to understand how people learn to make optimal decisions. " +
        "You are eligible for this study if you are fluent in English, have normal or corrected-to-normal vision, "  +
        "18-50 years old and if you do not receive psychiatric treatment or take medication for mental illness. </p>" +

        "<h3>2. Procedure and content of the study</h3>" +
        "<p align='left'> For this study, we invented the Fribble game." +
        " Fribble is a new type of animal you will meet during the experiment." +
        " In this game, you will learn what are the food preferences of different types of Fribbles." +
        " At the end, we will ask you for some basic demographic information (age, sex), and for your feedback on what you learned in the task." +
        "<br> <strong> Please only participate from a computer (no mobiles, no tablets) and if your screen is at least 13-inch diagonal. The experiment will only work on Chrome or Firefox. " +
        " Also know that you will need to remain in full screen mode for the entire experiment. If you exit full screen mode, there is no guarantee we can compensate you. </strong></p>" +

        "<h3>3. What will happen to the collected data?</h3>" +
        "<p align='left'> This study is a research project of the Max Planck Institute for Human Development, Berlin. The data collected will be used for research purposes only." +
        " Your Prolific ID will be stored separately from the study data. The study data will be stored under an individual code number." +
        " This code number is stored on a separate code list in a secure location and can only be accessed by a limited number of project members." +
        " The code list is the only link between your Prolific ID and your study data." +
        " After payment, the code list will be deleted, and it will no longer be possible to link the study data to your Prolific ID." +
        " The study data (but not Prolific ID) may be shared with cooperation partners and made publicly accessible via research data bases or " +
        "scientific publications (typically via the Internet)."+
        " The study data may also be used for new research questions going beyond the purposes of this particular study." +
        " Study data are only transferred or made publicly accessible without the associated Prolific IDs or any data which renders persons identifiable.</p>" +

        "<h3>4. Participation is voluntary </h3>" +
        "<p align='left'> Participation in this study is voluntary. You may withdraw from the study or withdraw your consent to data processing and usage any time before payment. " +
        "To do this, please contact grossman@mpib-berlin.mpg.de. Please note that after payment, it is no longer possible to link your Prolific ID to your study data, and therefore your data cannot be deleted anymore. " +

        "<h3>5. Session structure and Payment </h3>" +
        "<p align='left'> The experiment should take an hour overall (including short breaks, instructions and training). " +
        "You will be paid 8.5 GBP for the experiment and an additional bonus of up to 5 GBP depending on the amount of points you will earn." +
        "<p align='center'> Press any key to continue >>> <br>";

    } else {
      var consent_text = "<h2>Study Information and Statement of informed Consent</h2>" +
          "<h3>1. Aim of the study</h3>" +
          "<p align='left'> In this study we want to understand how people learn to make optimal decisions. " +
          "You are eligible for this study if you are fluent in English, have normal or corrected-to-normal vision, "  +
          "18-50 years old and if you do not receive psychiatric treatment or take medication for mental illness. </p>" +

          "<h3>2. Procedure and content of the study</h3>" +
          "<p align='left'> For this study, we invented the real-estate game." +
          " You will be trained to be a real estate agent and learn what is the best properties you should offer to different clients." +
          "At the end, we will ask you for some basic demographic information." +
          "<br> <strong> Please only participate from a computer (no mobiles, no tablets) and if your screen is at least 13-inch diagonal. The experiment will only work on Chrome or Firefox. " +
          " Also know that you will need to remain in full screen mode for the entire experiment. If you exit full screen mode, there is no guarantee we can compensate you. </strong></p>" +

          "<h3>3. What will happen to the collected data?</h3>" +
          "<p align='left'> This study is a research project of the Max Planck Institute for Human Development, Berlin. The data collected will be used for research purposes only." +
          " Your Prolific ID will be stored separately from the study data. The study data will be stored under an individual code number." +
          " This code number is stored on a separate code list in a secure location and can only be accessed by a limited number of project members." +
          " The code list is the only link between your Prolific ID and your study data." +
          " After payment, the code list will be deleted, and it will no longer be possible to link the study data to your Prolific ID." +
          " The study data (but not Prolific ID) may be shared with cooperation partners and made publicly accessible via research data bases or " +
          "scientific publications (typically via the Internet)."+
          " The study data may also be used for new research questions going beyond the purposes of this particular study." +
          " Study data are only transferred or made publicly accessible without the associated Prolific IDs or any data which renders persons identifiable.</p>" +

          "<h3>4. Participation is voluntary </h3>" +
          "<p align='left'> Participation in this study is voluntary. You may withdraw from the study or withdraw your consent to data processing and usage any time before payment. " +
          "To do this, please contact grossman@mpib-berlin.mpg.de. Please note that after payment, it is no longer possible to link your Prolific ID to your study data, and therefore your data cannot be deleted anymore. " +

          "<h3>5. Session structure and Payment </h3>" +
          "<p align='left'> The experiment should take an hour overall (including short breaks, instructions and training). " +
          "You will be paid 8 GBP for the experiment and an additional bonus of up to 5.5 GBP depending on the amount of points you will earn." +
          "<p align='center'> Press any key to continue >>> <br>";

      }
      return [consent_text]

    }

var dist_to_screen_txt = "<p class='demopara',align='left'> We would like to verify that your eyes are aproximately 60cm (23.6 inches) from the screen."+
                         " Please extend your right arm towards your screen and hold the palm of your hand straight at the center of the screen (as depicted in the picture below). <br>"+
                         " Make sure that <strong>your arm is as straight as possible </strong> (aproximately at 90 degrees to your body)."+
                         " You are welcome to adjust the chair and/or the location of the screen so you are as comfortable as possible. "+
                         " <strong> Please try to keep this distance from the screen for the rest of the experiment. </strong>"+
                         " Press any key to continue to task instructions. </p><br>"+
                         '<img src="stim/dist_to_screen.png" style="position: absolute; top: 80%; right: 50%; transform: translateY(0%)" />';

var OptimalPerceptuion_reminder = "<div style='position: fixed; top: 5px;line-height: normal;'>" +
                                  "Dont forget: as long as the fixation cross (<strong>+</strong>) is on the screen, please focus your gaze on it.</center> <br> <br>" +
                                  "Please realign your position - face the palm of your right hand on the hand-figure depicted on the right and make sure that <strong>your arm is as straight as possible </strong>. <br> <br>"


function get_instructions(simple_stim) {
  if (simple_stim){
      var instructions1_text = "<p class='demopara',align='left'> Welcome and thank you for participating in our experiment!<br>" +
          " In this experiment, you will learn to feed a new animal species - the FRIBBELS.<br>" +
          " Fribbles can differ in various elemetns: they have two possible leg shapes and two possible head shapes." +
          " The possible combinations of these variations - in legs and head shapes - lead to 4 possible Fribble types.<br>" +
          " Interestingly, different types of Fribbles like different kinds of food, and you will have to learn what type of food " +
          " pleases more what type of Fribble.<br>" +
          " On every trial, you will be presented with a Fribble. After seeing the Fribble type, you will be" +
          " presented with two optional food types you can offer it. Food items will also differ in 3 aspects:" +
          " they can come in two different shapes, textures and colors, leading to 8 possible food types.<br>" +
          " Press any key to continue reading the instructions >>> </p>";

      var instructions2_text = "<p class='demopara',align='left'> Below you can see one example Fribble and one example food type:<br>" +
          '<img src="stim/stim_examples.png" style="position: absolute; top: 80%; left: 50%; transform: translateX(-50%)" />' +
          " Press any key to continue reading the instructions >>> </p>";

      var instructions3_text =
          "<p class='demopara',align='left'>  <strong>Your goal is to earn as many points as possible.</strong> <br>" +
          " After having selected a food type for a Fribble type," +
          " you will get a number of points (0-100) that indicates how much the Fribble liked the food." +
          " To maximize the number of points you will recieve, you will have to learn about the food preferences of each Fribble type. <br>"
          " Note that not necessarily both Fribble's features (its head AND its legs) impact its food preference, <br>" +
          " but it could be that only one of those (either head or legs shape), impact the preference. <br>" +
          " Similarly, Fribbles may only care about some of the food features (shape, texture, color), while being indifferent to the rest." +
          " Press any key to continue reading the instructions >>> </p>";

      var instructions4_text = "<p class='demopara',align='left'> The feedback on each decision (0-100 points)," +
          " will be accumulated throughout the experiment." +
          " At the end of the experiment, the amount of points you have earned will be converted into money added " +
          " to your baseline payment (8 GBP) on participating in the experiment. " +
          " The maximal accumulated reward you can earn by always making the optimal decision is 5.5GBP.<br>" +
          " Overall, the experiment will take about an hour, with a short break introduced every ~10 minutes." +
          " Please remember that we won't be able to pay you if you do not complete the experiment or if you exit " +
          " the full screen mode before the experiment ends. <br>" +
          " Press any key to continue reading the instructions >>> </p>";

      var instructions5_text = "<p class='demopara',align='left'> Please make sure to keep your eyes at the cross in the center of the screen." +
          " Before a trial begins, the cross will turn yellow to remind you to focus your eyes on it.<br>" +
          " To select a food item on each trial, follow these key instructions:</p><br><br>" +
          '<img src="stim/key_instructions.png" style="position: absolute; top: 80%; left: 50%; transform: translateX(-50%)" />' +
          " Press any key to continue reading the instructions >>> </p>";

      var instructions6_text = "<p class='demopara',align='left'>  You will now do a short training (3 trials) to get familiarized with the task." +
        " Press any key to start training >>> </p>"

      var instructions_text_post_training = "<p class='demopara',align='left'> Great! you are ready to start the experiment." +
            " The experiment will take about 50 minutes, including short breaks.<br>" +
            " When you are ready, press any key to start the experiment >>></p>"

      } else {
        var instructions1_text = "<p class='demopara',align='left'> Welcome and thank you for participating in our experiment!<br>" +
            " In this experiment, you will train to be a great real-estate agent. " +
            " On every trial, you will be presented with a customer who is interested in purchasing a house." +
            " After seeing the customer, you will be presented with two optional houses you can offer her/him." +
            " Once you make a decision, you will recieve a feedback from your agency, indicating how pleased was the customer with your offer. <br><br>" +
            " Press any key to continue reading the instructions >>> </p>";

        var instructions2_text = "<p class='demopara',align='left'> Belowyou can see one example Fribble and one example food type:<br>" +
            '<img src="stim/example_stim.png" style="position: absolute; top: 80%; left: 50%; transform: translateX(-50%)" />' +
            " Press any key to continue reading the instructions >>> </p>";


        var instructions3_text =
            "<p class='demopara',align='left'>  Your goal is to earn as many points as possible. After having selected a property for a customer," +
            " you will get a number of points that indicate how much the customer liked the house, and accordingly" +
            " how much you did make from the deal. Note that this is entirely independent from the price of the " +
            " house itself! All you care about is to satisfy your customers, which is reflected in the number of points you earn. " +
            " This will require that you learn about the preferences of customers."
            " Press any key to continue reading the instructions >>> </p>";

        var instructions4_text = "<p class='demopara',align='left'> The feedback on each decision will range between 0 and 100 points," +
            " and will be accumulated throughout the experiment." +
            " At the end of the experiment, the amount of points you have earned will be converted into money added " +
            " to your baseline payment on participating in the experiment. " +
            " The maximal accumulated reward you can earn by always making the optimal decision is 5.5GBP.<br>" +
            " Overall, the experiment will take about 1 hour, with a short break introduced every ~10 minutes." +
            " Please remember that we won't be able to pay you if you do not complete the experiment or if you exit the full screen mode before the experiment ends. <br>" +
            " Press any key to continue reading the instructions >>> </p>";


        var instructions5_text = "<p class='demopara',align='left'> Please make sure to keep your eyes at the cross in the center of the screen." +
            " Before a trial begins, the cross will turn yellow to remind you to focus your eyes on it.<br>" +
            " To select a house on each trial, follow these key instructions:</p><br><br>" +
            '<img src="stim/key_instructions.png" style="position: absolute; top: 80%; left: 50%; transform: translateX(-50%)" />' +
            " Press any key to continue reading the instructions >>> </p>";

        var instructions6_text = "<p class='demopara',align='left'>  You will now do a short training (3 trials) to get familiarized with the task." +
          " Press any key to start training >>> </p>"

        var instructions_text_post_training = "<p class='demopara',align='left'> Great! you are ready to start the experiment." +
              " The experiment will take about 50 minutes, including short breaks.<br>" +
              " When you are ready, press any key to start the experiment >>></p>"

      }
      return [instructions1_text,instructions2_text,instructions3_text,instructions4_text,instructions5_text,instructions6_text,instructions_text_post_training]
    }



var outtext =
  '<p align="left">If you have any further questions about the study, please contact us. </p>'+
  '<p align="left"> Press <strong>N</strong> to get back to Prolific. <br>'+
  'It might take a few moments to redirect, during which you might see a blank screen. '+
  'Please do not close the window until you are redirected to Prolific otherwise we will not recieve your data and will not be able to approve you to the next session. </p> '


//not using
var debrief_text2 =  "The end. THANK YOU!";

//not using
var text_switch2D = "Great, you finsihed the first part of the experiment! <br> Now lets see how well you can identify trees with leaves AND fruits! <br> Press 'N' to start the next part";


// not using now
var SavingText =  "<p align='left'>We are shortly saving some of the data. The next part will start immediately after the data is saved. This can take up to 10 seconds.</p>"


// not using for now
var ExplicitBlock_instructions = "<p align='left'>Well done! You are almost done with the experiment! The next part is very short.<br>"+
                                 " We are interested if you remember which trees the generous gnomes liked more."+
                                 " We will show you a series of trees. For each tree, we will ask you to estimate how many gold coins this tree was previously associated with." +
                                // " More accurate estimations in this part will be rewarded. </p>"+ // !!!!!!!!!!!!!!!!!!! reward scale here!
                                "<p><center><b>We will now exit full-screen mode.</b></p>"+
                                "<p><center>Note: in the rare case of technical difficulty in the next part, you could also use the TAB key to navigate to the response box.</p>"+
                                "<p><center>Press <strong>N</strong> to move to the next part (minimum waiting time: 3 seconds)</p>"
