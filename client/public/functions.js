// get how many pixels are field of view
function angle2pix(PixInCM=100,ang=10,distfromscreen=60){
    //converts visual angles in degrees to pixels.
    var pixSize = 1/PixInCM //pixel size in CM
    var sz = 2*distfromscreen*Math.tan(Math.PI*ang/(2*180));  //image width in cm assuming 60cm distance
    return Math.round(sz/pixSize);} // returns the pixels needed for ang degrees

// sum up the minimal reward and the maximal reward
function get_sum_min_max_reward(prt_idx) {
  var min_reward=0;
  var max_reward=0;
  for (let i = 1; i < prt[prt_idx].length; i++) {
  min_reward = min_reward + Number(prt[prt_idx][i].min_reward);
  max_reward += Number(prt[prt_idx][i].max_reward);
  }
  return [min_reward,max_reward]
}

// send data to the ARC server
function send_data_to_ARC_server(){ // GoToProlific?
    var JSDATA = jsPsych.data.get().filter({doSave: true}).json()
    var expData = [JSON.parse(JSDATA)]
    const body = {data: expData} //, structure according to ARC
    postData(JSON.stringify(body))
}



// relocate to prolific
function SendToProlific(){
    if (prolific_on){location.replace("https://app.prolific.co/submissions/complete?cc="+completion_code);} // https://app.prolific.co/submissions/complete?cc=619A85D1
}

// wait ms miliseconds
function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
 }


var attempts = 0 , errorattempts = 0

var postData = function(data){
    $.ajax({
        async: false, // should still work, see https://stackoverflow.com/questions/11448011/jquery-ajax-methods-async-option-deprecated-what-now
        url: "https://realestator.exp.neurocode.mpib.org/submit",
        type: 'post',
        headers: {
            'Access-Control-Allow-Origin': '*' // not sure if this is needed
        },
        data : data,
        processData: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'JSON',
        success: function (output) {
            // whatever the data try 3 times
            attempts=attempts+1
            if (output.code==0){SendToProlific()};
            wait(2000) // wait 2 secodns
            // if didnt sent to prolific so the data was sent but other message from ARC than 0 - then try to send the data again up to 3 times
            if (attempts<3){postData(data)} // will not work after attempt 3.
            // then we try to send the error message itseld for 4th time attempt only
            if (attempts<4){postData(JSON.stringify(output),PARTIAL)}
            // if it got here that means it tries 3 times to ARC, got non 0 code, and then sent the error to ARC and was succsfull
            SendToProlific(); // send to prolific only if partial==false
        },// end success
        error: function (error) { // if the sent was not successgull, i.e. not even an ARC code
            wait(2000) // wait 3 secodns
            errorattempts=errorattempts+1
            if (errorattempts<3){postData(data)} //  will not work after attempt 3
            // then we try to send the error message itsels.
            if (errorattempts<4){postData(JSON.stringify(error))}
            // if everything failes, then send to prolific
            SendToProlific(); // send to prolific only if partial==false
        }// the whole process is not 100% perfect but will try maximum of 7 or 8 times which means ca 15 seconds and then send back to prolific.
       });
    }
